var FormView = require('ampersand-form-view');
var InputView = require('ampersand-input-view');
var CheckboxView = require('ampersand-checkbox-view');

var ExtendedInput = InputView.extend({
  template: require('../templates/partials/formInput.hbs')
});

module.exports = FormView.extend({
  fields: function () {
    return [
      new ExtendedInput({
        label: 'Container Type',
        name: 'containertype',
        value: this.model && this.model.containerType,
        placeholder: 'Container Type',
        required: true,
        parent: this
      }),
      new ExtendedInput({
        label: 'IAAS',
        name: 'iaas',
        value: this.model && this.model.iaas,
        placeholder: 'IAAS',
        required: true,
        parent: this
      }),
      new ExtendedInput({
        label: 'Region',
        name: 'region',
        value: this.model && this.model.region,
        placeholder: 'Region',
        required: true,
        parent: this
      }),
      new ExtendedInput({
        label: 'Region URL',
        name: 'regionUrl',
        value: this.model && this.model.regionUrl,
        placeholder: 'Region URL',
        required: true,
        parent: this
      }),
      new ExtendedInput({
        label: 'IP',
        name: 'ip',
        value: this.model && this.model.ip,
        placeholder: 'IP Address',
        required: true,
        parent: this
      }),
      new ExtendedInput({
        label: 'Disabled',
        name: 'iaas',
        value: this.model && this.model.iaas,
        placeholder: 'Disabled',
        required: false,
        parent: this
      }),
      new ExtendedInput({
        label: 'Name',
        name: 'name',
        value: this.model && this.model.name,
        placeholder: 'Name',
        required: false,
        parent: this
      }),
      new ExtendedInput({
        label: 'Proxy',
        name: 'proxy',
        value: this.model && this.model.proxy,
        placeholder: 'Proxy',
        required: false,
        parent: this
      }),
      new ExtendedInput({
        label: 'puAvailable',
        name: 'puAvailable',
        value: this.model && this.model.puAvailable,
        placeholder: 'PU Available',
        required: false,
        parent: this
      })
      /*
      new ExtendedInput({
        label: 'Coolness Factor',
        name: 'coolnessFactor',
        value: this.model && this.model.coolnessFactor,
        type: 'number',
        placeholder: '8',
        parent: this,
        tests: [
          function (val) {
            if (val < 0 || val > 11) return 'Must be between 0 and 11';
          },
          function (val) {
            if (!/^[0-9]+$/.test(val)) return 'Must be a number.';
          }
        ]
      }) */
    ];
  }
});
