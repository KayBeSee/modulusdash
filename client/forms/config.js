/* global $ */

/*
 * Modulus, Inc
 * Author: Shad Beard
 */

var FormView      = require('ampersand-form-view');
var InputView     = require('ampersand-input-view');

var ExtendedInput = InputView.extend({
    template: require('../templates/partials/form-inputs.hbs'),
});

// Helper function for changing property names to a human readable form
function toTitleCase(str) {
  str = str.replace(/_/g, ' ');
  return str.replace(/\w\S*/g, function(txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

/*
 *  This Input adds a 'Generate' button that will create a random password to the text field.
 */
var RandomInput = InputView.extend({
  template: require('../templates/partials/random-input.hbs'),
  events: {
    'click .generate': 'generate'
  },
  generate: function () {
    var g = this.generateRandomPassword();
    $(this.el).find('input.form-control').val(g);
  },
  generateRandomPassword: function () {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    var string_length = 24;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
    }
    return randomstring;
  },
  initialize: function (spec) {
    // Add a random password to all generate fields by default - for now!
    spec || (spec = {});
    this.tests = this.tests || spec.tests || [];
    this.on('change:type', this.handleTypeChange, this);
    // this.handleChange = this.handleChange.bind(this); Causing error...
    this.handleInputChanged = this.handleInputChanged.bind(this);
    var value = !spec.value && spec.value !== 0 ? '' : spec.value;
    this.startingValue = value;
    this.inputValue = this.generateRandomPassword() || value;
    this.on('change:valid change:value', this.reportToParent, this);
    this.on('change:validityClass', this.validityClassChanged, this);
    if (spec.autoRender) this.autoRender = spec.autoRender;
    if (spec.template) this.template = spec.template;
    if (spec.beforeSubmit) this.beforeSubmit = spec.beforeSubmit;
  }
});

// Read in the config file that generates the form fields
var json = require('../../config/ui.json');


// Create the form
module.exports = FormView.extend({
  fields: function () {
    var arr = [];
    var tmp;
    for (var i = 0; i < json.length; i++) {
      switch (json[i].type) {
        case 'string':
          tmp = new ExtendedInput({
            label: toTitleCase(json[i].key),
            name: json[i].key,
            value: this.model && this.model[json[i].key],
            placeholder: json[i].key,
            parent: this
          });
          arr.push(tmp);
          break;
        case 'random':
          console.log(this.model[json[i].key]);
          tmp = new RandomInput({
            label: toTitleCase(json[i].key),
            name: json[i].key,
            value: '',
            parent: this
          });
          arr.push(tmp);
          break;
        default:
          // Do Nothing - type doesn't exist, so don't render anything
      }
    }

    return arr;
  }
});
