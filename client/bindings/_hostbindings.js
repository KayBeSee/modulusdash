  module.exports = {
    'model.id': {
      hook: 'hostID'
    },
    'model.containerType': {
      hook: 'hostContainerType'
    },
    'model.deletedOn': {
      hook: 'hostDeletedOn'
    },
    'model.iaas': {
      hook: 'hostIAAS'
    },
    'model.ip': {
      hook: 'hostIP'
    },
    'model.name': {
      hook: 'hostName'
    },
    'model.proxy': {
      hook: 'hostProxy'
    },
    'model.puAvailable': {
      hook: 'hostPuAvailable'
    },
    // Not necessary because /pages/host-view.js adds to collection then renders collection.
    'model.puIds': {
      hook: 'hostPuIds',
    },
    'model.region': {
      hook: 'hostRegion'
    },
    'model.regionURL': {
      hook: 'hostRegionURL'
    },
    'model.editUrl': {
      type: 'attribute',
      hook: 'action-edit',
      name: 'href'
    },
    'model.viewUrl': {
      type: 'attribute',
      hook: 'hostID',
      name: 'href'
    },
    'model.puCount': {
      hook: 'hostNumberOfPus'
    },
    'model.prettyDisabled': {
      hook: 'disabledButton'
    }
  };