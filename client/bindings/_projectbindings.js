  module.exports = {
    'model.id': {
      hook: 'projectID'
    },
    'model.autoSSLRedirect': {
      hook: 'autoSSLRedirect'
    },
    'model.created_date': {
      hook: 'created_date'
    },
    'model.creator': {
      hook: 'creator'
    },
    'model.deployOptions.forceNpmInstall': {
      hook: 'deployOptions-forceNpmInstall'
    },
    'model.deployOptions.registry': {
      hook: 'deployOptions-registry'
    },
    'model.domain': {
      hook: 'domain'
    },
    'model.filesId': {
      hook: 'filesId'
    },
    'model.puCount': {
      hook: 'puCount'
    },
    'model.puIds': {
      hook: 'puIds'
    },
    'model.scaleOptions.iaas': {
      hook: 'scaleIaas'
    },
    'model.scaleOptions.region': {
      hook: 'scaleRegion'
    },
    'model.scaleOptions.instances': {
      hook: 'scaleInstances'
    },
    'model.servoSize': {
      hook: 'servoSize'
    },
    'model.status': {
      hook: 'status'
    },
    'model.storageLocation': {
      hook: 'storageLocation'
    },
    'model.pus': {
      hook: 'pus'
    },
    'model.addonVars': {
      hook: 'addonVars'
    },
    'model.addOns': {
      hook: 'addOns'
    },
    'model.envVars': {
      hook: 'envVars'
    },
    'model.customSSL': {
      hook: 'customSSL'
    },
    'model.customDomains': {
      hook: 'customDomains'
    },
    'model.images.build.label': {
      hook: 'images-build-label'
    },
    'model.images.build.name': {
      hook: 'images-build-name'
    },
    'model.images.build.stability': {
      hook: 'images-build-stability'
    },
    'model.images.build.type': {
      hook: 'images-build-type'
    },
    'model.images.build.userId': {
      hook: 'images-build-userId'
    },
    'model.images.build.tags': {
      hook: 'images-build-tags'
    },
    'model.images.build.createdAt': {
      hook: 'images-build-createdAt'
    },
    'model.images.build.official': {
      hook: 'images-build-official'
    },
    'model.images.build.id': {
      hook: 'images-build-id'
    },
    'model.images.run.label': {
      hook: 'images-run-label'
    },
    'model.images.run.name': {
      hook: 'images-run-name'
    },
    'model.images.run.stability': {
      hook: 'images-run-stability'
    },
    'model.images.run.type': {
      hook: 'images-run-type'
    },
    'model.images.run.userId': {
      hook: 'images-run-userId'
    },
    'model.images.run.tags': {
      hook: 'images-run-tags'
    },
    'model.images.run.createdAt': {
      hook: 'images-run-createdAt'
    },
    'model.images.run.official': {
      hook: 'images-run-official'
    },
    'model.images.run.id': {
      hook: 'images-run-id'
    },
    'model.viewUrl': {
      type: 'attribute',
      hook: 'projectID',
      name: 'href'
    }
  };