var PageView   = require('./base');           // grab base page view
var HostView = require('../views/host');  // grab host view

module.exports = PageView.extend({
  pageTitle: 'Hosts Collection',
  template: require('../templates/pages/hosts.hbs'),
  events: {
  },
  render: function () {
    var renderCount = 0;
    this.renderWithTemplate();
    this.renderCollection(this.collection, HostView, this.queryByHook('host-list'));
    this.on('hostView:render-complete', function () {
      if (++renderCount === this.collection.length) {
        $('#hosts').kendoGrid({ // jshint ignore:line
          sortable: true,
          mobile: true,
          pageable: true,
          allowCopy: true,
          filterable: true,
          columnResizeHandleWidth: 6,
          dataSource: {
            pageSize: 5,
            refresh: true,
            serverPaging: false
          }
        });
      }
    });
    if (!this.collection.length) {
      this.fetchCollection();
    }
  },

  fetchCollection: function () {
    this.collection.fetch();
    return false;
  },
  resetCollection: function () {
    this.collection.reset();
  }
});
