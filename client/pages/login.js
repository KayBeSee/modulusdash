var View = require('ampersand-view');

module.exports = View.extend({
  pageTitle: 'Here is a sample page',
  template: require('../templates/pages/login.hbs'),
  events: {
    'click [data-hook=submit]' : 'handleSubmitClick'
  },
  handleSubmitClick: function(event) {
    event.preventDefault();
    // me.authenticated = true;
    window.app.navigate('home');
  },
  render: function() {
    this.renderWithTemplate();
  }
});
