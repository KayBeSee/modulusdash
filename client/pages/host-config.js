/* global $ */

var View        = require('ampersand-view');
// var templates = require('../../templates');
var ConfigForm  = require('../forms/config');
var ConfigModel = require('../models/config');
var json = require('../../config/ui.json');

module.exports = View.extend({
  pageTitle: 'Configuration',
  template: require('../templates/pages/host-config.hbs'),
  events: {
    'change [data-hook="upload"]': 'handleFileSelect',
    'click [data-hook="download-json"]': 'handleDownloadJson',
    'click [data-hook="download-sup"]': 'handleDownloadSup',
    'click [data-hook="preview"]': 'handlePreview',
    // 'click [data-hook="apply"]': 'handleApply',
    'click [data-hook="default"]': 'handleDefault',
    // 'click .up': 'submit'
  },
  subviews: {
    forms: {
      container: '[data-hook=config-form]',
      // Add model to bind to here...
      prepareView: function (el) {
        console.log('this: ', this);
        // this.model = new ConfigModel();
        // console.log('model: ', this.model)
        return new ConfigForm({
          el: el,
          model: new ConfigModel(),
          submitCallback: function (data) {
            // This should be used for the apply button ???
            console.log('data: ', data);

            // Download the file to the users computer here...
            this.model.save(data, {
              wait: true,
              success: function (model, response, options) {
                console.log('success');
                console.log('Model: ', model);
                console.log('Response: ', response);
                console.log('Options: ', options);
                // window.location = '/'
              },
              error: function() {
                console.log('fail');
              }
            });
          }
        });
      }
    }
  },
  handleFileSelect: function () {
    // Get access to this var
    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
      window.alert('The File APIs are not fully supported in this browser.');
      return;
    }

    var input = $('.upload')[0].files[0];

    if (!input) {
      window.alert('Um, couldn\'t find the fileinput element.');
    } else {
      var reader = new FileReader();
      reader.readAsText(input);
      reader.onload = function (file) {
        // Get the parsed uploaded file from the FileReader
        var out = JSON.parse(file.target.result);

        // Loop over ui.json file and replace each $formElement value with the value in the uploaded file.
        for (var i = 0; i < json.length; i++) {
          var tmp = json[i].key;

          // Check only 'string' type so that generated passwords are not overwritten
          if (json[i].type === 'string') {
            var $formEl = $(this.forms.el).find('[name="' + json[i].key + '"]');
            $formEl.val(out[tmp]);
          }
        }
      }.bind(this);
    }
  },
  handleDownloadJson: function () {
    console.log('handling download button.');
    var output = {};

    // var defaults = require('../../config/default-cloud-config.json');

    // Loop over json and replace the input fields with the default values.
    for (var i = 0; i < json.length; i++) {
      var $formEl = $(this.forms.el).find('[name="' + json[i].key + '"]');
      var a = $formEl.val();

      output[json[i].key] = a;
    }

    // Post output back to the server for download ???
    console.log('output: ', output);
  },
  handleDownloadSup: function () {
    console.log('handle download supervisor');
  },
  handlePreview: function () {
    console.log('handle preview');
  },
  handleApply: function () {
    console.log('handle apply');
  },
  handleDefault: function () {
    // Read in the default cloud variables from a config file
    var defaults = require('../../config/default-cloud-config.json');

    // Loop over json and replace the input fields with the default values.
    for (var i = 0; i < json.length; i++) {
      var tmp = json[i].key;

      if (json[i].type === 'string') {
        var $formEl = $(this.forms.el).find('[name="' + json[i].key + '"]');
        $formEl.val(defaults[tmp]);
      }
    }
  }
});
