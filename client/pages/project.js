/* global app, alert */
var PageView = require('./base');
var projectBindings = require('../bindings/_projectBindings');
var fileListItem = require('../views/listitem');
var ServerDropDown = require('../views/server-drop-down');

module.exports = PageView.extend({
  pageTitle: 'view project',
  template: require('../templates/pages/project.hbs'),
  bindings: projectBindings,
  events: {
    'click [data-hook~=delete]': 'handleDeleteClick'
  },
  subviews: {
    dropdown: {
      container: '[data-hook=server-drop-down]',
      prepareView: function (el) {
        return new ServerDropDown({
          el         : el,
          parent     : this,
          collection : app.projects
        });
      }
    }
  },
  initialize: function (spec) {
    var self = this;
    app.projects.getOrFetch(spec.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a project with id: ' + spec.id);
      self.model = model;
    });
  },
  render: function(){
    this.renderWithTemplate();
    this.renderCollection(this.collection, fileListItem, this.queryByHook('filesFileList')); // Render PuID collection for list.
  },
  fetch: function(spec) {
    app.projects.getOrFetch(spec.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a model with id: ' + spec.id);
      self.model = model;
    });
  },
  handleDeleteClick: function () {
    this.model.destroy({success: function () {
      app.navigate('projects');
    }});
  }
});

