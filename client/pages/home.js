var PageView = require('./base');

module.exports = PageView.extend({
  pageTitle: 'home',
  template: require('../templates/pages/home.hbs'),
  render: function() {
    this.renderWithTemplate(this);

    setTimeout(function () {
      $("#datepicker").kendoDatePicker();
    }, 100);

    return this;
  }
});
