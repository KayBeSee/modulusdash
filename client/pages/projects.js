var PageView   = require('./base');           // grab base page view
var ProjectView = require('../views/project');  // grab host view

module.exports = PageView.extend({
  pageTitle: 'Projects Collection',
  template: require('../templates/pages/projects.hbs'),
  events: {
  },
  render: function () {
    var renderCount = 0;
    this.renderWithTemplate();
    this.renderCollection(this.collection, ProjectView, this.queryByHook('project-list'));
    this.on('projectView:render-complete', function () {
      if (++renderCount === this.collection.length) {
        $('#projects').kendoGrid({ // jshint ignore:line
          sortable: true,
          mobile: true,
          pageable: true,
          dataSource: {
            pageSize: 5,
            refresh: true,
            serverPaging: false
          }
        });
      }
    });

    if (!this.collection.length) {
      this.fetchCollection();
    }
  },
  fetchCollection: function () {
    this.collection.fetch();
    return false;
  },
  resetCollection: function () {
    this.collection.reset();
  }
});
