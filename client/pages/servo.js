/* global app, alert */
var PageView = require('./base');

module.exports = PageView.extend({
  pageTitle: 'view servo',
  template: require('../templates/pages/servo.hbs'),
  bindings: {
    'model.id': {
      hook: 'servoID'
    },
    'model.fileLocation': {
      hook: 'servoFileLocation'
    },
    'model.hostID': {
      hook: 'servoHostId'
    },
    'model.ip': {
      hook: 'servoIp'
    },
    'model.projectId': {
      hook: 'servoProjectId'
    },
    'model.size': {
      hook: 'servoSize'
    },
    'model.status': {
      hook: 'servoStatus'
    },
    'model.host.id': {
      hook: 'servoHostId'
    },
    'model.host.region': {
      hook: 'servoHostRegion'
    },
    'model.host.iaas': {
      hook: 'servoHostIaas'
    },
    'model.host.containerType': {
      hook: 'servoHostContainerType'
    },
  },
  events: {
    'click [data-hook=servoHostId]': 'navigateToHost'
  },
  initialize: function (spec) {
    var self = this;
    app.servo.getOrFetch(spec.id, {all: true}, function (err, model) {
      if (err) alert('couldnt finds a model with id: ' + spec.id);
      self.model = model;
    });
  },
  navigateToHost: function () {
    debugger;
    app.navigate('hosts/' + this.model.host.id);
  }
});