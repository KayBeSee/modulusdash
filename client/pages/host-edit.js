/* global app, alert */
var PageView = require('./base');
var HostForm = require('../forms/host');

module.exports = PageView.extend({
  pageTitle: 'edit host',
  template: require('../templates/pages/host-edit.hbs'),
  initialize: function (spec) {
    var self = this;
    app.hosts.getOrFetch(spec.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a model with id: ' + spec.id);
      self.model = model;
    });
  },
  subviews: {
    form: {
      // this is the css selector that will be the `el` in the
      // prepareView function.
      container: 'form',
      // this says we'll wait for `this.model` to be truthy.
      waitFor: 'model',
      prepareView: function (el) {
        var model = this.model;
        return new HostForm({
          el: el,
          model: this.model,
          submitCallback: function (data) {
            this.model.save(data, {
              wait: true,
              error: function(err) {
                console.log('error', err);
              },
              success: function () {
                debugger;
                //this.render.bind(data);
                app.navigate('hosts');
              }
            });
          }
        });
      }
    }
  }
});
