var PageView   = require('./base');           // grab base page view
var ServoView = require('../views/servo');  // grab host view

module.exports = PageView.extend({
  pageTitle: 'Servo Collection',
  template: require('../templates/pages/servos.hbs'),
  events: {
    'click [data-hook~=fetch]'   : 'fetchCollection',
    'click [data-hook~=reset]'   : 'resetCollection',
  },
  render: function () {
    this.renderWithTemplate();
    this.renderCollection(this.collection, ServoView, this.queryByHook('servo-list'));
    if (!this.collection.length) {
      this.fetchCollection();
    }
  },
  fetchCollection: function () {
    this.collection.fetch();
    return false;
  },
  resetCollection: function () {
    this.collection.reset();
  },

});
