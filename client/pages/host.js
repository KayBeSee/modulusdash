/* global app, alert */
var PageView = require('./base');
var hostBindings = require('../bindings/_hostbindings');
var PuIDItem = require('../views/listItem');
var ServerDropDown = require('../views/server-drop-down');

module.exports = PageView.extend({
  pageTitle: 'view host',
  template: require('../templates/pages/host.hbs'),
  bindings: hostBindings,
  events: {
    'click [data-hook=list-item]': 'navigateToPuID',
    'click [data-hook=disabledButton]': 'clickDisable',
    'click [data-hook=editButton]': 'clickEdit',
  },
  subviews: {
    dropdown: {
      container: '[data-hook=server-drop-down]',
      prepareView: function (el) {
        return new ServerDropDown({
          el         : el,
          parent     : this,
          collection : app.hosts
        });
      }
    }
  },
  initialize: function (spec) {
    var self = this;
    app.hosts.getOrFetch(spec.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a model with id: ' + spec.id);
      self.model = model;
      for(var i=0; i < self.model.puIds.length; i++){ // Populate PuIDs List
        self.collection.add([{
          listItem: self.model.puIds[i]
        }]);
      }
    });
    $('#puIDs').kendoGrid({ // jshint ignore:line
          sortable: true,
          mobile: true,
          pageable: true,
          allowCopy: true,
          filterable: true,
          columnResizeHandleWidth: 6,
          dataSource: {
            pageSize: 5,
            refresh: true,
            serverPaging: false
          }
        });
  },
  render: function(){
    this.renderWithTemplate();
    this.renderCollection(this.collection, PuIDItem, this.queryByHook('hostPuIDs')); // Render PuID collection for list.
  },
  fetch: function(spec) {
    app.hosts.getOrFetch(spec.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a model with id: ' + spec.id);
      self.model = model;
    });
  },
  navigateToPuID: function () {
    app.navigate('servos/' + this.model.host.id);
  },
  clickEdit: function () {
    app.navigate('hosts/' + this.model.id + '/edit');
  },
  clickDisable: function() {
    debugger;
    if(this.model.disabled){
      $("#disabledButton").removeClass("alert");
      $("#disabledButton").addClass("success");
      this.model.disabled = false;
    }
    else{
      $("#disabledButton").removeClass("success");
      $("#disabledButton").addClass("alert");
      this.model.disabled = true;
    }
    this.model.save({
      success: this.render.bind(this)
    });
  }
});
