/*global me, app*/
var Router = require('ampersand-router');
var HomePage = require('./pages/home');
var InfoPage = require('./pages/info');
var LoginView = require('./pages/login');

// Include Host Collections file
var HostsCollection = require('./pages/hosts');
var HostViewPage = require('./pages/host');
var HostEditPage = require('./pages/host-edit');
var HostConfigView = require('./pages/host-config');
// Servo View Page
var ServoViewPage = require('./pages/servo');
// Projects Pages
var ProjectsCollection = require('./pages/projects');
var ProjectViewPage = require('./pages/project');

var ListItems = require('./models/listItems');


module.exports = Router.extend({
  routes: {
    ''                : 'home',
    'login'           : 'login',
    'hosts'           : 'hosts',
    'hosts/config'    : 'hostConfig',
    'hosts/:id'       : 'host',
    'hosts/:id/edit'  : 'hostEdit',
    'servos'          : 'servos',
    'servos/:id'      : 'servo',
    'projects'        : 'projects',
    'projects/:id'    : 'project',
    'info'            : 'info',
    'logout'          : 'logout',
    '(*path)'         : 'catchAll'
  },

  // ------- ROUTE HANDLERS ---------
  home: function () {
    this.trigger('page', new HomePage({
      model: me
    }));
  },

  login: function() {
    this.trigger('page', new LoginView({
    }));
  },

  hostConfig: function () {
    this.trigger('page', new HostConfigView({
    }));
  },

  hosts: function () {
    this.trigger('page', new HostsCollection({
      model: me,
      collection: app.hosts
    }));
  },

  host: function (id) {
    this.trigger('page', new HostViewPage({
      id: id,
      collection: new ListItems(), // to display servo list
    }));
  },

  hostEdit: function (id) {
    this.trigger('page', new HostEditPage({
      id: id,
    }));
  },

  /* Not in use...may add later.
  servos: function () {
    this.trigger('page', new ServosCollection({
      model: me,
      collection: app.servos,
    }));
  }, */

  servo: function (id) {
    this.trigger('page', new ServoViewPage({
      id: id
    }));
  },

  projects: function () {
    this.trigger('page', new ProjectsCollection({
      model: me,
      collection: app.projects,
    }));
  },

  project: function (id) {
    this.trigger('page', new ProjectViewPage({
      id: id,
      collection: new ListItems() // to display file list
    }));
  },

  info: function () {
    this.trigger('page', new InfoPage({
      model: me
    }));
  },

  logout: function() {
    this.trigger('page', new LoginView({
    }));
  },

  catchAll: function () {
    this.redirectTo('');
  }
});
