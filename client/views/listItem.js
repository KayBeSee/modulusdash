var View = require('ampersand-view');

module.exports = View.extend({
  template: require('../templates/partials/listItem.hbs'),
  bindings: {
    "model.listItem" : {
      type: "text",
      hook: 'list-item'
    }
  }

  ,
  events: {
    'click [data-hook=list-item]': 'clickPuID'
  },
  initialize: function (spec) {
  },
  clickPuID: function () {
    app.navigate('servos/' + this.model.listItem);
  }
});