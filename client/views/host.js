var View = require('ampersand-view');
var hostBindings = require('../bindings/_hostbindings');
var Events = require('ampersand-events');

module.exports = View.extend({
  template: require('../templates/partials/host.hbs'),
  bindings: hostBindings,
  events: {
    'click [data-hook=list-item]': 'clickPuID',
  },
  initialize: function (spec) {
    var self = this;
    app.hosts.getOrFetch(spec.model.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a host with id: ' + spec.id);
      self.model = model;
      Events.createEmitter(self);
      self.parent.trigger('hostView:render-complete').bind(this);
    });
  },
  clickPuID: function () {
    app.navigate('servos/' + this.model.listItem);
  }
});