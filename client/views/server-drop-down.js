/* global $ */

var View = require('ampersand-view');
var ServerDropDownItem = require('./server-drop-down-item');

module.exports = View.extend({
  template: require('../templates/partials/server-drop-down.hbs'),
  events: {
    'change  [data-hook=server-drop-down]': 'handleDropDown'
  },
  render: function() {
    this.renderWithTemplate();
    this.renderCollection(this.collection, ServerDropDownItem, this.queryByHook('server-drop-down'));
    if (!this.collection.length) {
      this.fetchCollection();
    }
  },
  fetchCollection: function () {
    this.collection.fetch();
    return false;
  },
  handleDropDown: function (e) {
    var urlParts = window.app.router.history.location.pathname.split('/');
    urlParts.pop();
    var redirectURL = urlParts.join('/') + '/' + $(e.currentTarget).val();
    window.app.navigate(redirectURL);
  },
});
