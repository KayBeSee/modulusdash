/* global app, alert */
var PageView = require('ampersand-view');
var projectBindings = require('../bindings/_projectbindings');
var Events = require('ampersand-events');

module.exports = PageView.extend({
  pageTitle: 'view project',
  template: require('../templates/partials/project.hbs'),
  bindings: projectBindings,
  events: {
  },
  initialize: function (spec) {
    var self = this;
    app.projects.getOrFetch(spec.model.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a project with id: ' + spec.id);
      self.model = model;
      Events.createEmitter(self);
      self.parent.trigger('projectView:render-complete').bind(this);
    });
  },
});

