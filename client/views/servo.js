/* global app, alert */
var PageView = require('ampersand-view');

module.exports = PageView.extend({
  pageTitle: 'view servo',
  template: require('../templates/pages/servoView.hbs'),
  bindings: {
    'model.id': {
      hook: 'servoID'
    },
    'model.fileLocation': {
      hook: 'servoFileLocation'
    },
    'model.hostID': {
      hook: 'servoHostId'
    },
    'model.ip': {
      hook: 'servoIp'
    },
    'model.projectId': {
      hook: 'servoProjectId'
    },
    'model.size': {
      hook: 'servoSize'
    },
    'model.status': {
      hook: 'servoStatus'
    },
    'model.host.id': {
      hook: 'servoHostId',
      name: 'href',
      type: 'attribute'
    },
    'model.host.region': {
      hook: 'servoHostRegion'
    },
    'model.host.iaas': {
      hook: 'servoHostIaas'
    },
    'model.host.containerType': {
      hook: 'servoHostContainerType'
    },
  },
  events: {
    'click [data-hook=servoHostId]': 'navigateToHost'
  },
  initialize: function (spec) {
    var self = this;
    app.servo.get(spec.model.id, {all: true}, function (err, model) {
      if (err) alert('couldnt find a model with id: ' + spec.id);
      self.model = model;
    });
  },
  navigateToHost: function () {
    app.navigate('hosts/' + model.host.id);
  }
});
