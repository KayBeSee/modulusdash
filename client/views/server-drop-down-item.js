var View = require('ampersand-view');

module.exports = View.extend({
  template: require('../templates/partials/server-drop-down-item.hbs'),
  render: function() {
    this.renderWithTemplate();
  }
});
