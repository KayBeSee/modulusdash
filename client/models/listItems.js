var Collection = require('ampersand-rest-collection');
var listItem = require('./listItem');


module.exports = Collection.extend({
    model: listItem
});
