var AmpersandModel = require('ampersand-model');


module.exports = AmpersandModel.extend({
  props: {                 // Default properties from host API call
    id: {
        type: 'string',
    },
    autoSSLRedirect: {
        type:'boolean',
    },
    created_date: {
        type: 'string',
    },
    creator:{
        type: 'string',
    },
    deployOptions: {            // Not sure what datatype this is.
        forceNpmInstall: {
            type: 'string',
        },
        registry: {
            type: 'string',
        },
    },
    domain: {
        type: 'string',
    },
    filesId: {
        type: 'string',
    },
    puCount: {
        type: 'number',
    },
    puIds: {
        type: 'array',
    },

    // scaleOptions Object properties
    scaleOptions: {
        iaas: {
            type: 'string',
        },
        region: {
            type: 'string',
        },
        instances: {
            type: 'number',
        },
    },
    servoSize: {
        type: 'number',
    },
    status: {
        type: 'string',
    },
    storageLocation: {
        type: 'string',
    },
    pus: {
        type: 'array',
    },
    addonVars: {
        type: 'array',
    },
    addOns: {
        type: 'array',
    },
    envVars: { // array of objects
        type: 'array',
    },
    customSSL: {
        type: 'array',
    },
    customDomains: {
        type: 'array',
    },
    images: {
        build: {
            label: {
                type: 'string',
            },
            name: {
                type: 'string',
            },
            stability: {
                type: 'string',
            },
            type: {
                type: 'string',
            },
            userId: {
                type: 'number',
            },
            tags: {
                type: 'array'
            },
            createdAt: {
                type: 'string',
            },
            official: {
                type: 'boolean',
            },
            id: {
                type: 'string',
            },
        },
        run: {
            label: {
                type: 'string',
            },
            name: {
                type: 'string',
            },
            stability: {
                type: 'string',
            },
            type: {
                type: 'string',
            },
            userId: {
                type: 'number',
            },
            tags: {
                type: 'array'
            },
            createdAt: {
                type: 'string',
            },
            official: {
                type: 'boolean',
            },
            id: {
                type: 'string',
            },
        },
      },
    },
  session: {
        selected: ['boolean', true, false]
    },
  derived: {
        editUrl: {
            deps: ['id'],
            fn: function () {
                return '/projects/' + this.id + '/edit';
            }
        },
        viewUrl: {
            deps: ['id'],
            fn: function () {
                return '/projects/' + this.id;
            }
        }
    }
});
