var Collection = require('ampersand-rest-collection');
var Servo = require('./servo');


module.exports = Collection.extend({
    model: Servo,
    url: '/api/servos'
});
