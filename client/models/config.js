var Model = require('ampersand-model');
var json = require('../../config/ui.json');

var props = {};

for (var i = 0; i < json.length; i++) {
  props[json[i].key] = {
    type: 'string'
  };
}

console.log('Props: ', props);

module.exports = Model.extend({
  urlRoot: '/api/apply',
  props: props
});
