var AmpersandModel = require('ampersand-model');

module.exports = AmpersandModel.extend({
  props: {                 // Default properties from host API call
    id: {
        type: 'string',
    },
    fileLocation: {
        type:'string',
    },
    hostID: {
        type: 'string',
    },
    ip:{
        type: 'string',
    },
    projectId: {
        type: 'string',
    },
    size: {
        type: 'string',
    },
    status: {
        type: 'string',
    },
    host: {
        id: {
            type: 'string',
        },
        region: {
            type: 'string',
        },
        iaas: {
            type: 'string',
        },
        containerType: {
            type: 'string',
        }
    }
  },
  session: {
        selected: ['boolean', true, false]
    },
  derived: {
        editUrl: {
            deps: ['id'],
            fn: function () {
                return '/servos/' + this.id + '/edit';
            }
        },
        viewUrl: {
            deps: ['id'],
            fn: function () {
                return '/servos/' + this.id;
            }
        }
    }
});
