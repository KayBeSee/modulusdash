var Collection = require('ampersand-rest-collection');
var Project = require('./project');


module.exports = Collection.extend({
    model: Project,
    url: '/api/projects'
});
