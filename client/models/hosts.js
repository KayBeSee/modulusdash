var Collection = require('ampersand-rest-collection');
var Host = require('./host');


module.exports = Collection.extend({
    model: Host,
    url: '/api/hosts'
});
