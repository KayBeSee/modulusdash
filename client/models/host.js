var AmpersandModel = require('ampersand-model');


module.exports = AmpersandModel.extend({
  props: {                 // Default properties from host API call
    id: {
        type: 'string',
    },
    containerType: {
        type:'string',
    },
    deletedOn: {
        type: 'string',
    },
    disabled: {
        type: 'boolean',
        required: true,
        default: false,
    },
    iaas:{
        type: 'string',
    },
    ip: {
        type: 'string',
    },
    name: {
      type: 'string',
    },
    proxy: {
        type: 'string',
    },
    puAvailable: {
        type: 'number',
    },
    puIds: {
        type: 'array',
    },
    region: {
        type: 'string',
    },
    regionUrl: {
        type: 'string',
    }
  },
  session: {
        selected: ['boolean', true, false]
    },
  derived: {
    idPt1: {
      deps: ['id'],
      fn: function() {
        var pos = this.id.indexOf("-");
        return this.id.slice(0,pos);
      }
    },
    prettyDisabled: {
      deps: ['disabled'],
      fn: function() {
        if(this.disabled){
          return "Disabled";
        }
        else {
          return "Enabled";
        }
      }
    },
    puCount: {
      deps: ['puIds'],
      fn: function() {
        return this.puIds.length;
      }
    }
  }
});
