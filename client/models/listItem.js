var AmpersandModel = require('ampersand-model');


module.exports = AmpersandModel.extend({
  props: {                 // Default properties from host API call
    listItem: {
        type: 'string',
    },
  }
});
