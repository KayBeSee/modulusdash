var path         = require('path');
process.config = require('./config');

var fs           = require('fs');
var bodyParser   = require('body-parser');
var compress     = require('compression');
var cookieParser = require('cookie-parser');
var config       = require('getconfig');
var express      = require('express');
var helmet       = require('helmet');
var Moonboots    = require('moonboots-express');
var semiStatic   = require('semi-static');
var serveStatic  = require('serve-static');
var stylizer     = require('stylizer');
var librarian    = require('librarian');

var sass         = require('node-sass');

librarian.init(process.config.CONGRESS_URL, process.config.CONGRESS_PORT);

var app          = express();

// a little helper for fixing paths for various environments
var fixPath = function (pathString) {
  return path.resolve(path.normalize(pathString));
};

// -----------------
// Configure express
// -----------------
app.use(compress());
app.use(serveStatic(fixPath('public')));

// we only want to expose tests in dev
if (config.isDev) {
  app.use(serveStatic(fixPath('test/assets')));
  app.use(serveStatic(fixPath('test/spacemonkey')));
}

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// in order to test this with spacemonkey we need frames
if (!config.isDev) {
  app.use(helmet.xframe());
}
app.use(helmet.xssFilter());
app.use(helmet.nosniff());

app.set('view engine', 'jade');

// -----------------
// for Hosts example
// Set up our little demo API
// -----------------
app.get('/api/hosts', function(req, res) {
  librarian.host.getAll(process.config.CONGRESS_AUTH_TOKEN, function(err, hosts) {
    if (err) return res.status(400).send(err);
    res.status(200).send(hosts);
  });
});

app.get('/api/hosts/:id', function(req, res) {
  librarian.host.get(req.params.id, process.config.CONGRESS_AUTH_TOKEN, function(err, hosts) {
    if (err) return res.status(400).send(err);
    res.status(200).send(hosts);
  });
});

app.put('/api/hosts/:id', function(req, res) {
  console.log(req.body);
  librarian.host.update(req.body, process.config.CONGRESS_AUTH_TOKEN, function(err, hosts) {
    if (err) return res.status(400).send(err);
    res.status(200).send(hosts);
  });
});
// app.delete('/api/hosts/:id', api.delete);
// app.put('/api/hosts/:id', api.update);
// app.post('/api/hosts', api.add);

// -----------------
// for Servos example
// Set up our little demo API
// -----------------
app.get('/api/servos', function(req, res) {
  librarian.servo.getAll(process.config.CONGRESS_AUTH_TOKEN, function(err, servos) {
    if (err) return res.status(400).send(err);
    res.status(200).send(servos);
  });
});

app.get('/api/servos/:id', function(req, res) {
  librarian.servo.get(req.params.id, process.config.CONGRESS_AUTH_TOKEN, function(err, servos) {
    if (err) return res.status(400).send(err);
    res.status(200).send(servos);
  });
});
// app.delete('/api/servos/:id', servoApi.delete);
// app.put('/api/servos/:id', servoApi.update);
// app.post('/api/servos', servoApi.add);

// -----------------
// for Projects example
// -----------------
app.get('/api/projects', function(req, res) {
  librarian.project.getAll(process.config.CONGRESS_AUTH_TOKEN, function(err, projects) {
    console.log(err, projects[0]);
    if (err) return res.status(400).send(err);
    res.status(200).send(projects);
  });
});

app.get('/api/projects/:id', function(req, res) {
  librarian.project.find({projectId: req.params.id}, process.config.CONGRESS_AUTH_TOKEN, function(err, projects) {
    if (err) return res.status(400).send(err);
    res.status(200).send(projects);
  });
});

// app.delete('/api/projects/:id', projectApi.delete);
// app.put('/api/projects/:id', projectApi.update);
// app.post('/api/projects', projectApi.add);

// -----------------
// for Users
// -----------------
app.get('/api/users', function(req, res) {
	librarian.user.getAll(process.config.CONGRESS_AUTH_TOKEN, function(err, users) {
		if (err) return res.status(400).send(err);
		res.status(200).send(users);
	});
});

app.get('/api/users/count', function(req, res) {
	librarian.user.getCount(process.config.CONGRESS_AUTH_TOKEN, function(err, count) {
		if (err) return res.status(400).send(err);
		res.status(200).send(count);
	});
});

app.delete('/api/users/delete/:id', function(req, res) {
	librarian.user.delete(req.params.id, process.config.CONGRESS_AUTH_TOKEN, function(err, success) {
		if (err) return res.status(400).send(err);
		res.status(200).send(success);
	});
});

app.get('/api/users/:id', function(req,res) {
	librarian.user.get(req.params.id, process.config.CONGRESS_AUTH_TOKEN, function(err, user) {
		if (err) return res.status(400).send(err);
		res.status(200).send(user);
	});
});

app.get('/api/users/:id/roles', function(req, res) {
	librarian.user.getRoles(req.params.id, process.config.CONGRESS_AUTH_TOKEN, function(err, roles) {
		if (err) return res.status(400).send(err);
		res.status(200).send(roles);
	});
});

app.get('/api/users/:id/activity', function(req, res) {
	librarian.user.activity(req.params.id, process.config.CONGRESS_AUTH_TOKEN, function(err, activity) {
		if (err) return res.status(400).send(err);
		res.status(200).send(activity);
	});
});


// -----------------
// Enable the functional test site in development
// -----------------
if (config.isDev) {
  app.get('/test*', semiStatic({
    folderPath: fixPath('test'),
    root: '/test'
  }));
}


// -----------------
// Set our client config cookie
// -----------------
app.use(function (req, res, next) {
  res.cookie('config', JSON.stringify(config.client));
  next();
});


// ---------------------------------------------------
// Configure Moonboots to serve our client application
// ---------------------------------------------------
new Moonboots({
  moonboots: {
    jsFileName: 'ampersand-test-app',
    cssFileName: 'ampersand-test-app',
    main: fixPath('client/app.js'),
    developmentMode: process.config.developmentMode,
    libraries: [
      fixPath('public/js/jquery.min.js'),
      fixPath('public/js/kendo.all.min.js')
    ],
    stylesheets: [
      fixPath('public/css/style.css'),
      fixPath('public/css/kendo.common.min.css')
    ],
    browserify: {
      debug: process.config.debug,
      transforms: ['browserify-handlebars']
    },
    beforeBuildCSS: function (done) {
      sass.render({
        file: fixPath('scss/style.scss'),
      }, function (err, result) {
        if (err) throw new Error(err.message);

        fs.writeFile(fixPath('public/css/style.css'), result.css, done);
      });
    }
  },
  server: app
});


// listen for incoming http requests on the port as specified in our config
app.listen(config.http.port);
