var moonboots  = require('./moonboots');
var ui         = require('./ui.json');
var supervisor = require('./supervisor.in.json');

module.exports = {
  moonboots: moonboots,
  ui: ui,
  supervisor: supervisor
}
