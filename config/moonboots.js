var path = require('path');
var sass = require('node-sass');
var fs   = require('fs');

var appDir = path.join(__dirname, '../client');
var cssDir = path.join(__dirname, '../public/styles');
var cssSourceDir = path.join(__dirname, '../client/styles');

var isDev = (process.env.NODE_ENV !== 'production');

module.exports = {
  appPath: '/{p*}',
  moonboots: {
    jsFileName: 'kulla',
    cssFileName: 'kulla',
    main: path.join(appDir, '/app.js'),
    developmentMode: isDev,
    libraries: [
      path.join(__dirname, '../bower/jquery/dist/jquery.min.js'),
      path.join(__dirname, '../bower/bootstrap-sass/assets/javascripts/bootstrap.min.js'),
      path.join(__dirname, '../bower/i18next/i18next.js')
    ],
    stylesheets: [
      path.join(cssDir, '/app.css')
    ],
    browserify: {
      debug: false,
      transforms: ['hbsfy'] // This is what makes handlebars work
    },
    beforeBuildCSS: function(done) {
      sass.render({
        file: path.join(cssSourceDir, '/app.scss'),
        outputStyle: 'compressed',
        includePaths: [
          'bower',
          'client/styles',
          'bower/bootstrap-sass/assets/stylesheets'
        ],
        success: function (results) {
          fs.writeFile(path.join(cssDir, '/app.css'), results.css, function (err) {
            if (err) throw err;
            done();
          })
        },
        error: function (error) {
          console.log(error);
          done(error);
        }
      })
    }
  }
}
